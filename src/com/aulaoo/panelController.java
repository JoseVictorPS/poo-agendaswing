package com.aulaoo;

import java.awt.*;
import java.awt.event.ActionEvent;
//import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
//import java.util.*;

public class panelController extends JPanel implements ListSelectionListener {

    private JSplitPane painel;
    private JList lista;
    private JPanel dadosPessoais;
    private Agenda agenda;
    private JLabel nome;
    private JTextField nomeTxt;
    private JLabel telefone;
    private JTextField telTxt;
    private JLabel detalhes;
    private JTextArea detTxtArea;
    private JButton adiciona;
    private JButton remove;
    private JButton atualiza;

    public panelController() {
        agenda = new Agenda();

        lista = new JList(agenda.getNomes());
        lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lista.addListSelectionListener(this);

        JScrollPane listaScroll = new JScrollPane(lista);

        dadosPessoais = new JPanel();
        dadosPessoais.setLayout(new FlowLayout());

        painel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                listaScroll, dadosPessoais);
        painel.setOneTouchExpandable(false);
        painel.setDividerLocation(250);

        listaScroll.setMinimumSize(new Dimension(250, 50));
        dadosPessoais.setMinimumSize(new Dimension(400, 50));

        painel.setPreferredSize(new Dimension(650, 200));
        //updateDadosPessoais();

        nome = new JLabel("Nome:");
        dadosPessoais.add(nome);

        nomeTxt = new JTextField(25);
        dadosPessoais.add(nomeTxt);

        telefone = new JLabel("Telefone:");
        dadosPessoais.add(telefone);

        telTxt = new JTextField(23);
        dadosPessoais.add(telTxt);

        detalhes = new JLabel("Detalhes:");
        dadosPessoais.add(detalhes);

        detTxtArea = new JTextArea(7, 23);
        detTxtArea.setLineWrap(true);
        detTxtArea.setWrapStyleWord(true);
        dadosPessoais.add(detTxtArea);

        adiciona = new JButton("Adiciona");
        dadosPessoais.add(adiciona);

        remove = new JButton("Remove");
        dadosPessoais.add(remove);

        atualiza = new JButton("Atualiza");
        dadosPessoais.add(atualiza);

        adiciona.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                adicionaPessoa(e);
            }
        });

        remove.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                removePessoa(e);
            }
        });

        atualiza.addActionListener(new java.awt.event.ActionListener(){
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                atualizaPessoa(e);
            }
        });
    }

    public void valueChanged(ListSelectionEvent e) {
        JList lista = (JList)e.getSource();
        if(lista.getSelectedIndex() != -1) {
            updateDadosPessoais(lista.getSelectedIndex());
        }
    }

    public JSplitPane getSplitPane() {
        return painel;
    }

    private void adicionaPessoa(ActionEvent e) {
        Pessoa p = new Pessoa(nomeTxt.getText(),
                telTxt.getText(), detTxtArea.getText());
        if(notInLista(p))agenda.insert(p);
    }

    private void removePessoa(ActionEvent e) {
        if(lista.getSelectedIndex()!= -1){
            agenda.remove(lista.getSelectedIndex());
        }
    }

    private void atualizaPessoa(ActionEvent e) {
        if(lista.getSelectedIndex() != -1) {
            Pessoa p = new Pessoa(nomeTxt.getText(),
                    telTxt.getText(), detTxtArea.getText());
            agenda.update(p, lista.getSelectedIndex());
        }
    }

    private void updateDadosPessoais(int index) {
        nomeTxt.setText(agenda.getContatos().get(index).getNome());
        telTxt.setText(agenda.getContatos().get(index).getTelefone());
        detTxtArea.setText(agenda.getContatos().get(index).getDetalhe());
    }

    private boolean notInLista(Pessoa p) {
        for(Pessoa tst : agenda.getContatos()) {
            if(tst.equals(p)) return false;
        }
        return true;
    }

}
