package com.aulaoo;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public static void createAndShowGUI() {
        JFrame janela = new JFrame("Agenda");
        janela.setResizable(false);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panelController controlador = new panelController();
        janela.getContentPane().add(controlador.getSplitPane());
        janela.pack();
        janela.setVisible(true);
    }

}
