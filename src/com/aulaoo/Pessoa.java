package com.aulaoo;

public class Pessoa {
    private String nome;
    private String telefone;
    private String detalhe;

    public Pessoa(String n, String tel, String det) {
        nome = n;
        telefone = tel;
        detalhe = det;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(String detalhe) {
        this.detalhe = detalhe;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Pessoa) {
            Pessoa p = (Pessoa)o;
            if(nome.equals(p.nome) &&
                    telefone.equals(p.telefone) &&
                    detalhe.equals(p.detalhe))
                return true;
            return false;
        }
        return false;
    }
}
