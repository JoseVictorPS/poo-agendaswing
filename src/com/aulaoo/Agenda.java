package com.aulaoo;

import javax.swing.*;
import java.util.LinkedList;

public class Agenda {
    private LinkedList<Pessoa> contatos;
    private DefaultListModel<String> nomes;

    public Agenda() {
        contatos = new LinkedList<>();
        nomes = new DefaultListModel<>();
    }

    public LinkedList<Pessoa> getContatos() {
        return contatos;
    }

    public DefaultListModel<String> getNomes() {
        return nomes;
    }

    public void insert(Pessoa p) {
        contatos.add(p);
        nomes.addElement(p.getNome());
    }

    public void remove(int index) {
        contatos.remove(index);
        nomes.remove(index);
    }

    public void update(Pessoa p, int index) {
        contatos.set(index, p);
        nomes.set(index, p.getNome());
    }
}
